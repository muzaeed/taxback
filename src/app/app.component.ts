import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  parentData: string = '';
  passData() {
    this.parentData = "Look what I got from parent";
  }
}
